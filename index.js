const axios = require('axios');
const cheerio = require('cheerio');

exports.handler = async (event) => {
    // Check if 'colorId' is provided in the query parameters
    if (!event.queryStringParameters || !event.queryStringParameters.colorId) {
        return {
            statusCode: 400,
            body: JSON.stringify({ error: 'Color ID is missing in query parameters' }),
        };
    }

    // Extract 'colorId' from query parameters
    const colorId = event.queryStringParameters.colorId;

    // Construct the URL with the dynamic 'colorId'
    const url = `https://mycolor.space/?hex=%23${colorId}&sub=1`;
    try {
        const response = await axios.get(url);
        const responseHtml = response.data;

        const $ = cheerio.load(responseHtml);
        const categoriesHexValues = {};

        // Iterate over each color palette
        $('.color-palette').each((index, element) => {
            // Get the category name
            const category = $(element).find('h2').text().trim();

            // Get the hex values for this category
            const hexValues = $(element).find('.color-box .color').map((index, element) => {
                return $(element).attr('style').match(/background:(.*?);/)[1];
            }).get();

            categoriesHexValues[category] = hexValues;
        });

        return {
            statusCode: 200,
            body: JSON.stringify(categoriesHexValues),
            headers: {
                'Content-Type': 'application/json'
            }
        };
    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify({ error: 'Internal server error' }),
        };
    }
};
